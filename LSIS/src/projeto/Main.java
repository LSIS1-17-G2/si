/* Licenciatura em Engenharia de Sistemas - 2ºano - 2016/17
   Laboratórios de Sistemas I - 2º semestre

   Grupo 2: 1130094	RICARDO GUILHERME TAVARES DE SOUSA
            1140453	DIOGO JOÃO COSTA SILVA
            1141157	SIMÃO FORTE DE MAGALHÃES
            1141432	CLÁUDIA SOFIA RIBEIRO GONÇALVES
            1150594	BEBIANA FERREIRA PINTO
            1150711	MARIANA CELESTE MARTINS CORREIA
*/

package projeto;

import java.io.IOException;
import java.util.Formatter;
import java.util.InputMismatchException;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.util.Scanner;


public class Main {
    
    static Scanner in = new Scanner(System.in).useDelimiter("\n");
    static Formatter out = new Formatter(System.out);
    public static Connection conn;
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
        
        
        try{
            conn=LigacaoSQL.getConnection();    
    
        }catch (Exception e){
        e.printStackTrace();
        }
    
        int opcao;
        int opcao1;
        int opcao2;
    
        do {
            opcao = menu();
            
                    switch (opcao) {
                case 0:
                    System.out.println("Fim!");
                    break;
                    
                case 1:{ 
                    do{
                    System.out.println("\n\n***Informação acerca das Equipas ***");
                    opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar uma equipa***");
                            Equipas.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar a informação de uma equipa***");
                            Equipas.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar a informação de uma equipa***");
                            Equipas.eliminarLinha(); 
                            break;
                        
                        default:
                            System.out.println("Opção inválida!"); 
                            System.out.println("\n\n***Informação acerca das Equipas ***");
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break;
                    
                   
                case 2:{
                    do{
                     System.out.println("\n\n***Informação sobre os Elementos de uma Equipa ***");
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar a informação relativa a um elemento***");
                            Elementos.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar a informação relativa a um elemento***");
                            Elementos.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar a informação relativa a um elemento***");
                            Elementos.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break;
                 
                 
                case 3:{
                    do{
                     System.out.println("\n\n***Informação sobre o Robô de uma Equipa ***");
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar a informação relativa a um robô***");
                            Robo.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar a informação relativa a um robô***");
                            Robo.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar a informação relativa a um robô***");
                            Robo.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                    
                  
                case 4:{
                    
                    do{
                     System.out.println("\n\n***Informação acerca dos sensores***");
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar informação relativa a um sensor***");
                            Sensor.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar a informação relativa a um sensor***");
                            Sensor.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar a informação relativa a um sensor***");
                            Sensor.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                    
                case 5:{
                    
                    do{
                     System.out.println("\n\n***Informação acerca dos sensores associados***"); 
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar a informação relativa a um sensor associado a um robô***");
                            SensoresAssociados.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar a informação relativa a um sensor associado a um robô***");
                            SensoresAssociados.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar a informação relativa a um sensor associado a um robô***");
                            SensoresAssociados.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                    
                
                case 6:{
                    do{
                        System.out.println("\n\n***Informação acerca das provas***"); 
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar informação relativa a uma prova***");
                            Prova.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar informação relativa a uma prova***");
                            Prova.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar informação relativa a uma prova***");
                            Prova.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                
                case 7:{
                     do{
                         System.out.println("\n\n***Informação acerca da competição***"); 
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar informação relativa a uma competição***");
                            Competicao.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar informação relativa a uma competição***");
                            Competicao.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar informação relativa a uma competição***");
                            Competicao.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                
                case 8:{
                    do{
                        System.out.println("\n\n***Informação acerca da classificação***"); 
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar informação relativa a uma classificação***");
                            Classificacao.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar informação relativa a uma classificação***");
                            Classificacao.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar informação relativa a uma classificação***");
                            Classificacao.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                
                case 9:{
                    do{
                        System.out.println("\n\n***Informação acerca dos participantes na prova***"); 
                     opcao1=menu1();
                    switch(opcao1){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Adicionar informação relativa a um participante na prova***");
                            Participou.adicionarLinha();
                            break;
                        case 2:
                            System.out.println("\n\n***Alterar informação relativa a um participante na prova***");
                            Participou.alterarLinha();
                            break;
                        case 3:
                            System.out.println("\n\n***Eliminar informação relativa a um participante na prova***");
                            Participou.eliminarLinha(); 
                            break;
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao1=menu1();
                         break;     
                    }
                    }while(opcao1!=0);
                }
                break; 
                
                case 10:{
                    do{
                        opcao2=menu2();
                    switch(opcao2){
                        case 0:
                            System.out.println("Fim!");
                            break;
                        case 1:
                            System.out.println("\n\n***Informação existente relativa as Equipas***");
                            mostrarTabelas.mostrarTabelaEquipas();
                            break;
                        case 2:
                            System.out.println("\n\n***Informação existente relativa aos Elementos***");
                            mostrarTabelas.mostrarTabelaElementos();
                            break;
                        case 3:
                            System.out.println("\n\n***Informação existente relativa aos Robôs***");
                            mostrarTabelas.mostrarTabelaRobo();
                            break;
                        case 4:
                            System.out.println("\n\n***Informação existente relativa aos Sensores***");
                            mostrarTabelas.mostrarTabelaSensor();
                            break;
                        case 5:
                            System.out.println("\n\n***Informação existente relativa aos Sensores Associados***");
                            mostrarTabelas.mostrarTabelaSensoresAssociados(); 
                            break;
                        case 6:
                            System.out.println("\n\n***Informação relativa as Provas***");
                            mostrarTabelas.mostrarTabelaProva();
                            break;  
                        case 7:
                            System.out.println("\n\n***Informação existente relativa à Competição***");
                            mostrarTabelas.mostrarTabelaCompeticao();
                            break;
                        case 8:
                            System.out.println("\n\n***Informação existente relativa as Classificações***");
                            mostrarTabelas.mostrarTabelaClassificacao();
                            break;
                        case 9:
                            System.out.println("\n\n***Informação existente relativa as Participações***");
                            mostrarTabelas.mostrarTabelaParticipou();
                            break;     
                        default:
                            System.out.println("Opção inválida!"); 
                            opcao2=menu2();
                         break;     
                    }
                    }while(opcao2!=0);
                     
                }
                break;
                
                
                 default:
                    System.out.println("Opção inválida!");     
                            
    
            }
        }while(opcao!=0);
    }

    private static int menu (){
        String m ="\n\n* * * Competição Robô Bombeiro * * *\n" 
                +"\n 0- Sair"
                + "\n 1- Gerir Equipas"
                + "\n 2- Gerir Elementos"
                + "\n 3- Gerir Robô"
                + "\n 4- Gerir Sensor" 
                + "\n 5- Gerir Sensores Associados" 
                + "\n 6- Gerir Prova" 
                + "\n 7- Gerir Competição" 
                + "\n 8- Gerir Classificação" 
                + "\n 9- Gerir Participou" 
                + "\n\n 10- Visualizar" 
                + "\n\n Escolha uma opção:\n ";
        int op = 0;
        out.format(m);
        try {
            op = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Deve inserir um inteiro!");
        }
        return op;
}
    
    private static int menu1 (){
        
        String m ="\n * * * Escolha uma das seguintes opções * * *\n" 
                + "\n 0- Sair"
                + "\n 1- Adicionar"
                + "\n 2- Alterar"
                + "\n 3- Eliminar"    
                + "\n\n Escolha uma opção:\n ";
        
        int op = 0;
        out.format(m);
        try {
            op = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Deve inserir um inteiro!");
        }
        return op;
}
    
    private static int menu2 (){
        
        String m ="\n * * * Pretende visualizar a informação relativa a que tabela? * * *\n" 
                +"\n 0- Sair"
                + "\n 1- Equipas"
                + "\n 2- Elementos"
                + "\n 3- Robô"
                + "\n 4- Sensor" 
                + "\n 5- Sensores Associados" 
                + "\n 6- Prova" 
                + "\n 7- Competição" 
                + "\n 8- Classificação" 
                + "\n 9- Participou" 
                + "\n\n Escolha uma opção:\n ";
        
        int op = 0;
        out.format(m);
        try {
            op = in.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Deve inserir um inteiro!");
        }
        return op;
}

} 