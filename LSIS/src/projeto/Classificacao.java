package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Classificacao {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Equipa`, `ID_Prova`, `Pontuacao_Total`
            
            System.out.println("\nQual o ID da equipa?");
            String id_equipa = in.nextLine();
        
            System.out.println("\nQual o ID da prova?");
            String id_prova = in.nextLine();
            
            System.out.println("\nQual a pontuação total?");
            String pontuacao_total=in.nextLine();                           
            
                                   
            
            // inserir informação
            statement.executeUpdate("INSERT INTO classificacao (ID_Equipa,ID_Prova,Pontuacao_Total) " + " VALUES ('"+ id_equipa +"','" + id_prova +"',"+ pontuacao_total +")");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_sensor
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nPontuacao_Total\n");    

                
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID da prova em que pretende fazer essa alteração?");
            String id_prova= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE classificacao SET Pontuacao_Total = " + novo + " WHERE ID_Prova = '" + id_prova+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nPontuacao_Total\n");
                                
            
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM classificacao WHERE Pontuacao_Total" +  " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}
