package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Equipas {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `Nome`, `ID_Equipa`, `Instituição`, `País`, `Classe`
                    
            System.out.println("\nQual o nome da equipa?");
            String nome=in.nextLine();                           
        
            System.out.println("\nQual o ID da equipa?");
            String ID_Equipa=in.nextLine();
        
            System.out.println("\nQual o nome da instituicao?");
            String instituicao= in.nextLine();
        
            System.out.println("\nQual o país?");
            String pais=in.nextLine();
        
            System.out.println("\nA que classe pertence? Responda Juvenil, Walking, Junior ou Senior");
            String classe=  in.nextLine();
        
   
            // inserir informação
            statement.executeUpdate("INSERT INTO equipas (Nome,ID_Equipa,Instituicao,Pais,Classe) " + " VALUES ('"+ nome +"','" + ID_Equipa +"','" + instituicao +"','" + pais +"','" + classe +"')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, idEquipa
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("\nColunas sujeitas a alterações: \nNome, ID_Equipa, Instituição, País, Classe\n");    

            System.out.println("\nQual o nome da coluna que pretende alterar?");
            String nomeColuna=in.nextLine();                           
        
            System.out.println("\nPara que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\nQual o ID da equipa em que pretende fazer essa alteração?");
            String idEquipa= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE equipas SET " + nomeColuna + " = ' " + novo + "'" + " WHERE ID_Equipa = '" + idEquipa+"'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir a coluna e o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nNome, ID_Equipa, Instituição, País, Classe");
                    
            System.out.println("\nQual o nome da coluna?");
            String coluna=in.nextLine();                           
        
            System.out.println("\nQual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM equipas WHERE " +  coluna + " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
     }
             
    }
            
                               
        
           
    

    
    
    
    
    

