package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class SensoresAssociados {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Robo`,`ID_Sensor`, `n_sensor´
            
            System.out.println("\nQual o ID do robô?");
            String id_robo = in.nextLine();
        
            System.out.println("\nQual o ID do sensor?");
            String id_sensor = in.nextLine();
            
            System.out.println("\nQual o número de sensores?");
            String n_sensor=in.nextLine();                           
        
                        
            
            // inserir informação
            statement.executeUpdate("INSERT INTO sensoresAssociados (ID_Robo,ID_Sensor,n_sensor) " + " VALUES ('"+ id_robo +"','" + id_sensor +"',"+ n_sensor +")");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_sensor
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nn_sensor\n");    

                
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID do sensor em que pretende fazer essa alteração?");
            String id_sensor= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE sensoresAssociados SET n_sensor = " + novo + " WHERE ID_Sensor = '" + id_sensor+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nn_sensor\n");
                    
            
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM sensoresAssociados WHERE n_sensor" +  " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}
