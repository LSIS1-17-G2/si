package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;



public class Elementos {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: ID_Elemento`, ´ID_Equipa´, `Nome`, `Idade`, `E-mail`, `Telefone`
            System.out.println("\nQual o ID do elemento?");
            String ID_Elemento = in.nextLine();
        
            System.out.println("\nQual o ID da equipa?");
            String ID_Equipa=in.nextLine();                           
            
            System.out.println("\nQual o nome do elemento?");
            String nome= in.nextLine();
        
            System.out.println("\nQual a idade do elemento?");
            String idade=in.nextLine();
        
            
            System.out.println("\nQual o email do elemento?");
            String email=in.nextLine();
        
            System.out.println("\nQual o número do telefone do elemento?");
            String telefone= in.nextLine();
        
   
            // inserir informação
            statement.executeUpdate("INSERT INTO elementos (ID_Elemento,ID_Equipa,Nome,Idade,Email,Telefone) " + " VALUES ('"+ ID_Elemento + "','" + ID_Equipa + "','" + nome+"','" + idade +"','" + email +"'," + telefone +")");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: nomeColuna, novo, id
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nID_Elemento, Nome, Idade, Email, Telefone\n");    

            System.out.println("\nQual o nome da coluna que pretende alterar?");
            String nomeColuna=in.nextLine();                           
        
            System.out.println("\nPara que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\nQual o ID do Elemento em que pretende fazer essa alteração?");
            String id= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE elementos SET " + nomeColuna + " = '" + novo + "'" + " WHERE ID_Elemento = '" + id +"'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\nInserir a coluna e o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nID_Elemento, Nome, Idade, Email, Telefone\n");
                    
            System.out.println("\nQual o nome da coluna?");
            String coluna=in.nextLine();                           
        
            System.out.println("\nQual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM elementos WHERE " +  coluna + " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
     }
}