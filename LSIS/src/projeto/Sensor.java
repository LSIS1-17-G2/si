package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Sensor {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Sensor`, `Descricao´
            
            System.out.println("\nQual o ID do sensor?");
            String ID_Sensor = in.nextLine();
        
            System.out.println("\nQual a descrição do sensor?");
            String descricao=in.nextLine();                           
        
                        
            
            // inserir informação
            statement.executeUpdate("INSERT INTO sensor (ID_Sensor,Descricao) " + " VALUES ('"+ ID_Sensor +"','" + descricao +"')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_sensor
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nID_Sensor, Descricao\n");    

            System.out.println("\n Qual o nome da coluna que pretende alterar?");
            String nomeColuna=in.nextLine();                           
        
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID do sensor em que pretende fazer essa alteração?");
            String id_sensor= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE sensor SET " + nomeColuna + " = ' " + novo + "'" + " WHERE ID_Sensor = '" + id_sensor+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir a coluna e o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nID_Sensor e Descricao\n");
                    
            System.out.println("\n Qual o nome da coluna?");
            String coluna=in.nextLine();                           
        
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM sensor WHERE " +  coluna + " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}

