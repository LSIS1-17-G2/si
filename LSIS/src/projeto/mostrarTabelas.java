package projeto;
//import projeto.LigacaoSQL.conn;
import java.sql.*;
import static projeto.Main.conn;


public class mostrarTabelas
{

 
    public static void mostrarTabelaEquipas(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM equipas";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String Nome = rs.getString("Nome");
                String ID_Equipa = rs.getString("ID_Equipa");
                String Instituicao = rs.getString("Instituicao");
                String Pais = rs.getString("Pais");
                String Classe = rs.getString("Classe");
                
                // imprime os resultados
                System.out.format("%s, %s, %s, %s, %s\n", Nome, ID_Equipa, Instituicao, Pais, Classe);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaElementos(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM elementos";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Elemento = rs.getString("ID_Elemento");
                String ID_Equipa = rs.getString("ID_Equipa");
                String Nome = rs.getString("Nome");
                String Idade = rs.getString("Idade");
                String Email = rs.getString("Email");
                String Telefone = rs.getString("Telefone");
                
                // imprime os resultados
                System.out.format("%s, %s, %s, %s, %s, %s\n", ID_Elemento, ID_Equipa, Nome, Idade, Email, Telefone);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaRobo(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM robo";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Robo = rs.getString("ID_Robo");
                String ID_Equipa = rs.getString("ID_Equipa");
                String Tipo_Locomocao = rs.getString("Tipo_Locomocao");
                String Dimensoes = rs.getString("Dimensoes");
                String Metodo_Apagar_Chama = rs.getString("Metodo_Apagar_Chama");
                
                
                // imprime os resultados
                System.out.format("%s, %s, %s, %s, %s\n", ID_Robo, ID_Equipa, Tipo_Locomocao, Dimensoes, Metodo_Apagar_Chama);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
        
    public static void mostrarTabelaCompeticao(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM competicao";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Prova = rs.getString("ID_Prova");
                String ID_Equipa = rs.getString("ID_Equipa");
                String Tempo1 = rs.getString("Tempo1");
                String Tempo2 = rs.getString("Tempo2");
                String Tempo3 = rs.getString("Tempo3");
                String Volta_pos_inicial = rs.getString("Volta_pos_inicial");
                String Penalizacao = rs.getString("Penalizacao");
                String Pontuacao_Total = rs.getString("Pontuacao_Total");
                
                // imprime os resultados
                System.out.format("%s, %s, %s, %s, %s %s, %s, %s\n", ID_Prova, ID_Equipa, Tempo1, Tempo2, Tempo3, Volta_pos_inicial, Penalizacao, Pontuacao_Total);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaClassificacao(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM classificacao";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Equipa = rs.getString("ID_Equipa");
                String ID_Prova = rs.getString("ID_Prova");
                String Pontuacao_Total = rs.getString("Pontuacao_Total");
                
                // imprime os resultados
                System.out.format("%s, %s, %s\n",  ID_Equipa, ID_Prova, Pontuacao_Total);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaParticipou(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM participou";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Equipa = rs.getString("ID_Equipa");
                String ID_Prova = rs.getString("ID_Prova");
                                
                // imprime os resultados
                System.out.format("%s, %s\n", ID_Equipa, ID_Prova);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaProva(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM prova";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Prova = rs.getString("ID_Prova");
                String ID_Equipa = rs.getString("ID_Equipa");
                                
                // imprime os resultados
                System.out.format("%s, %s\n", ID_Prova, ID_Equipa);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaSensor(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM sensor";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Sensor = rs.getString("ID_Sensor");
                String Descricao = rs.getString("Descricao");
                                
                // imprime os resultados
                System.out.format("%s, %s\n", ID_Sensor, Descricao);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    public static void mostrarTabelaSensoresAssociados(){
        try{
        
            Statement statement = (Statement) conn.createStatement();
     
            // mostrar a tabela Equipas completa
            String query = "SELECT * FROM sensoresassociados";

            // create the java statement
            Statement st = conn.createStatement();
      
            // execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);
      
            // iterate through the java resultset
            while (rs.next()){
                String ID_Robo = rs.getString("ID_Robo");
                String ID_Sensor = rs.getString("ID_Sensor");
                String n_sensor = rs.getString("n_sensor");
                
                
                // imprime os resultados
                System.out.format("%s, %s, %s\n", ID_Robo, ID_Sensor, n_sensor);
            }
            st.close();
        }catch (Exception e){
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    
    
}
