package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Participou {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Equipa`,`ID_Prova´
            
            System.out.println("\nQual o ID da equipa?");
            String id_equipa = in.nextLine();
        
            System.out.println("\nQual o ID da prova?");
            String id_prova = in.nextLine();
            
               
            // inserir informação
            statement.executeUpdate("INSERT INTO participou (ID_Equipa,ID_Prova) " + " VALUES ('"+ id_equipa +"','" + id_prova +"')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
       /* Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_sensor
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nID_Prova\n");    

                
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID da prova em que pretende fazer essa alteração?");
            String id_prova= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE prova SET ID_Prova = '" + novo + "' WHERE ID_Prova = '" + id_prova+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }*/
       
       System.out.println("Não pode alterar chaves externas");
       
    }    
    
       
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            
            System.out.println("\n Qual o ID da prova cuja linha quer eliminar?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM participou WHERE ID_Prova" +  " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}