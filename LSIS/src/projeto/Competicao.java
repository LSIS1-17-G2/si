package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Competicao {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Prova`, `ID_Equipa`, `Tempo1`, `Tempo2`, `Tempo3`, `Volta_pos_inicial`, `Penalizacao`, `Pontuacao_Total`
           
            System.out.println("\nQual o ID da prova?");
            String id_prova = in.nextLine();
        
            System.out.println("\nQual o ID da equipa?");
            String id_equipa=in.nextLine();  
        
            System.out.println("\nTempo 1? hh:mm");
            String tempo1=in.nextLine();
            
            System.out.println("\nTempo 2? hh:mm");
            String tempo2=in.nextLine();
            
            System.out.println("\nTempo 3? hh:mm");
            String tempo3=in.nextLine();
            
            System.out.println("\nO robo volta à posição inicial? Responda Sim ou Nao");
            String volta_pos=in.nextLine();
            
            System.out.println("\nTeve penalizações? hh:mm");
            String penalizacao=  in.nextLine();
            
            System.out.println("\nQual a pontuação total?");
            String pontuacao=  in.nextLine();
        
   
            // inserir informação
            statement.executeUpdate("INSERT INTO competicao (ID_Prova,ID_Equipa,Tempo1,Tempo2,Tempo3,Volta_pos_inicial,Penalizacao,Pontuacao_Total) " + " VALUES ('"+ id_prova +"','" + id_equipa +"','" + tempo1 +"','" + tempo2 +"','" + tempo3 +"','" + volta_pos +"','" + penalizacao+"','" + pontuacao+"')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novoDado, id_prova
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nTempo1, Tempo2, Tempo3, Volta_pos_inicial, Penalizacao, Pontuacao_Total\n");    

            System.out.println("\n Qual o nome da coluna que pretende alterar?");
            String nomeColuna=in.nextLine();                           
        
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID da prova em que pretende fazer essa alteração?");
            String id_prova= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE competicao SET " + nomeColuna + " = ' " + novo + "'" + " WHERE ID_Prova = '" + id_prova+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir a coluna e o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nTempo1, Tempo2, Tempo3, Volta_pos_inicial, Penalizacao, Pontuacao_Total\n");
                    
            System.out.println("\n Qual o nome da coluna?");
            String coluna=in.nextLine();                           
        
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM competicao WHERE " +  coluna + " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}

