package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Prova {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Robo`,`ID_Sensor`, `n_sensor´
            
            System.out.println("\nQual o ID da prova?");
            String id_prova = in.nextLine();
        
            System.out.println("\nQual o ID da equipa?");
            String id_equipa = in.nextLine();
            
               
            // inserir informação
            statement.executeUpdate("INSERT INTO prova (ID_Prova,ID_Equipa) " + " VALUES ('"+ id_prova +"','" + id_equipa +"')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_sensor
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nID_Prova\n");    

                
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID da prova em que pretende fazer essa alteração?");
            String id_prova= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE prova SET ID_Prova = '" + novo + "' WHERE ID_Prova = '" + id_prova+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir o valor respetivo, para eliminar a linha correspondente\n"
                    +"Coluna sujeita a alteração: \nID_Prova\n");
                    
            
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM prova WHERE ID_Prova" +  " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}
