package projeto;

import com.mysql.jdbc.Statement;
import java.sql.SQLException;
import static projeto.Main.conn;
import java.util.Scanner;


public class Robo {
    
    public static void adicionarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: `ID_Robo`, `ID_Equipa`, `Tipo_locomocao`, `Dimensoes`, `Metodo_Apagar_Chama`
            
            System.out.println("\nQual o ID do robô?");
            String ID_Robo = in.nextLine();
        
            System.out.println("\nQual o ID da equipa?");
            String ID_Equipa=in.nextLine();                           
        
            System.out.println("\nQual o tipo de Locomoção?");
            String tipo_locomocao=in.nextLine();
        
            System.out.println("\nQuais as dimensões do robô? --*--");
            String dimensoes= in.nextLine();
        
            System.out.println("\nMétodo Apagar Chama? Responda Ventoinha ou Água");
            String metodo=in.nextLine();
            
            
        
   
            // inserir informação
            statement.executeUpdate("INSERT INTO robo (ID_Robo,ID_Equipa,Tipo_Locomocao,Dimensoes,Metodo_Apagar_Chama) " + " VALUES ('"+ ID_Robo +"','" + ID_Equipa +"','" + tipo_locomocao +"','" + dimensoes +"','" + metodo + "')");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    
    }
    
    public static void alterarLinha() throws SQLException{
          
        Scanner in = new Scanner(System.in);
        
        try{
        
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: Nome coluna, novo, id_robo
            //perguntar a coluna q quer alterar. mostrar possibilidades
            System.out.println("Colunas sujeitas a alterações: \nID_Robo, Tipo_Locomocao, Dimensoes e Metodo_Apagar_Chama\n");    

            System.out.println("\n Qual o nome da coluna que pretende alterar?");
            String nomeColuna=in.nextLine();                           
        
            System.out.println("\n Para que valor/texto quer alterar?");
            String novo=in.nextLine();
        
            System.out.println("\n Qual o ID do robo em que pretende fazer essa alteração?");
            String id_robo= in.nextLine();
                             

            // inserir informação
            statement.executeUpdate("UPDATE robo SET " + nomeColuna + " = ' " + novo + "'" + " WHERE ID_Robo = '" + id_robo+"'");                   
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }    
    
    public static void eliminarLinha() throws SQLException{  
        
        Scanner in = new Scanner(System.in);
        
        try{        
      
            // create a Statement from the connection
            Statement statement = (Statement) conn.createStatement();


            // PERGUNTAR AO UTILIZADOR: que linha quer eliminar
            System.out.println("\n Inserir a coluna e o valor respetivo, para eliminar a linha correspondente\n"
                    +"Colunas sujeitas a alterações: \nID_Robo, ID_Equipa, Tipo_Locomocao, Dimensoes e Metodo_Apagar_Chama\n");
                    
            System.out.println("\n Qual o nome da coluna?");
            String coluna=in.nextLine();                           
        
            System.out.println("\n Qual o valor/texto da célula respetiva?");
            String celula=in.nextLine();      
                 
            // eliminar informação
            statement.executeUpdate("DELETE FROM robo WHERE " +  coluna + " = '" + celula + "'");
            statement.close();
        }catch (Exception e){
            System.out.println("Falhou");
            e.printStackTrace();
        }
    }
    
    
    
    
}

