package animacao1;


import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class S2  {

    JFrame j = new JFrame();
    DrawPanel d = new DrawPanel();
    
    public S2() {

        initUI();
        
    }

    private void initUI() {
        
        j.setTitle("Máquina de Estados");
        j.setSize(1000, 750);
        j.setLocationRelativeTo(null);
        j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        j.add(d);
        j.setVisible(true);
    }
     
    class DrawPanel extends JPanel {

    private void doDrawing(Graphics g) throws InterruptedException {
        

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.blue);

        
            Dimension size = getSize();
            Insets insets = getInsets();

            int w = size.width - insets.left - insets.right;
            int h = size.height - insets.top - insets.bottom;

            //DESENHAR ESTADOS
        //S1
        g2d.setColor(new Color(70, 67, 123));
        g2d.drawOval(20, 224, 90, 90);    
        String s1= "S1"; 
        g2d.drawString(s1, 55, 270);
        //S2
        g2d.drawOval(220, 60, 90, 90);
        String s2= "S2";
        g2d.drawString(s2, 255, 110);
        //S3
        g2d.drawOval(414, 120, 90, 90);
        String s3= "S3";
        g2d.drawString(s3, 450, 165);
        //S4
        g2d.drawOval(220, 330, 90, 90);
        String s4= "S4";
        g2d.drawString(s4, 255, 380);
        //S5
        g2d.drawOval(525, 320, 90, 90);
        String s5= "S5";
        g2d.drawString(s5, 560, 365);
        //S6
        g2d.drawOval(720, 80, 90, 90);
        String s6= "S6";
        g2d.drawString(s6, 755, 130);
        //S7
        g2d.drawOval(680, 355, 90, 90);
        String s7= "S7";
        g2d.drawString(s7, 715, 400);
        //S8
        g2d.drawOval(570, 170, 90, 90);
        String s8= "S8";
        g2d.drawString(s8, 605, 215);
        
        
        //LIGAÇÕES ENTRE OS ESTADOS                
        //S1 -> S3
        g2d.setColor(new Color(63, 121, 186));
        g2d.drawLine(100, 237, 416, 174);
        String l_13= "1";//ação
        g2d.drawString(l_13, 110, 220);
        
        //S1 -> S5
        g2d.drawLine(110, 267, 526, 372);
        String l_15= "2";//ação
        g2d.drawString(l_15, 120, 261);
        
        //S1 -> S4
        g2d.drawLine(100, 297, 220, 375);
        String l_14= "3"; //ação
        g2d.drawString(l_14, 115, 300);
        
        //S2 -> S3
        g2d.drawLine(304, 129, 416, 156);
        String l_23= "1"; //ação
        g2d.drawString(l_23, 360, 140);
        
        //S2 -> S5
        g2d.drawLine(297, 137, 538, 333);
        String l_25= "2"; //ação
        g2d.drawString(l_25, 459, 256);
        
        //S3 -> S2
        g2d.drawLine(427, 133, 310, 112);
        String l_32= "2/3"; //ação
        g2d.drawString(l_32, 370, 115);
        
        //S5 -> S2
        g2d.drawLine(287, 146, 524, 354);
        String l_52= "1/3"; //ação
        g2d.drawString(l_52, 400, 271);
        
        //S4 -> S2
        g2d.drawLine(239, 337, 239, 140);
        String l_42= "1/2"; //ação
        g2d.drawString(l_42, 210, 170);
        
        
        
        //S2 -> S4
        g2d.drawLine(281, 150, 281, 335);
        String l_24= "3"; //ação
        g2d.drawString(l_24, 285, 240);
        
        //S4 -> S5
        g2d.drawLine(310, 385, 529, 388);
        String l_45= "10"; //ação
        g2d.drawString(l_45, 404, 370);
        
        //S3 -> S5
        g2d.drawLine(491, 200, 556, 319);
        String l_35= "10"; //ação
        g2d.drawString(l_35, 514, 235);
        
        //S7 -> S8
        g2d.drawLine(700, 362, 644, 250);
        String l_78= "9"; //ação
        g2d.drawString(l_78, 670, 278);
        
        //S6 -> S7
        g2d.drawLine(758, 170, 731, 355);
        String l_67= "8"; //ação
        g2d.drawString(l_67, 755, 225);
        
        //S2 -> S6
        g2d.drawLine(310, 93, 733, 93);
        String l_26= "7"; //ação
        g2d.drawString(l_26, 650, 90);
        
        //S2 -> S7
        g2d.drawLine(220, 99, 10, 99);
        String l_27= "11"; //ação
        g2d.drawString(l_27, 110, 80);
        g2d.drawLine(10, 100, 10, 440);
        g2d.drawLine(10, 440, 706, 440);
        
        //S7 -> S7
        g2d.drawLine(757, 430, 800, 430);
        String l_7= "6"; //ação
        g2d.drawString(l_7, 790, 380);
        g2d.drawLine(800, 430, 800, 390);
        g2d.drawLine(800, 390, 770, 390);
        
        //TEXTO
        String a= "Estados:"; 
        g2d.drawString(a, 35, 470);
        
        String b= "S1 - Fase Inicial - Desligado"; 
        g2d.drawString(b, 35, 490);
        
        String c= "S2 - Parado"; 
        g2d.drawString(c, 35, 510);
        
        String d= "S3 - Rotação à direita"; 
        g2d.drawString(d, 35, 530);
        
        String e= "S4 - Rotação à Esquerda"; 
        g2d.drawString(e, 35, 550);
        
        String f= "S5  - Andar para a frente"; 
        g2d.drawString(f, 35, 570);
        
        String o= "S6 - Apagar chama"; 
        g2d.drawString(o, 35, 590);
        
        String i= "S7 - Ler caminho"; 
        g2d.drawString(i, 35, 610);
        
        String j= "S8 - Posição original"; 
        g2d.drawString(j, 35, 630);
        
        String a1= "Ações:"; 
        g2d.drawString(a1, 466, 470);
        
        String b1= "1 - Detetar d>f e d>e"; 
        g2d.drawString(b1, 466, 490);
        
        String c1= "2 - Detetar e>f e e>d"; 
        g2d.drawString(c1, 466, 510);
        
        String d1= "3 - Detetar f>d e f>e"; 
        g2d.drawString(d1, 466, 530);
        
        String e1= "4 - Deteção de parede a frente"; 
        g2d.drawString(e1, 466, 550);
        
        String f1= "5 - Virar lado oposto"; 
        g2d.drawString(f1, 466, 570);
        
        String h1= "6 - Ler caminho inverso"; 
        g2d.drawString(h1, 466, 590);
        
        String i1= "7 - Detetar chama"; 
        g2d.drawString(i1, 466, 610);
        
        String j1= "8 - Detetar fogo apagado "; 
        g2d.drawString(j1, 466, 630);
        
        String k1 = "9 - Caminho invertido totalmente lido "; 
        g2d.drawString(k1, 466, 650);
        
        String m1= "10 - Andar para a frente"; 
        g2d.drawString(m1, 466, 670);
        
        String n1= "11 - Prepara leitura"; 
        g2d.drawString(n1, 466, 690);
        

             
    
        
        
        //DESTACAR S2
        
        g2d.setColor(new Color(42, 179, 231));
        g2d.fillOval(220, 60, 90, 90);
        g2d.setColor(new Color(70, 67, 123));
        g2d.drawString(s2, 255, 110);
  }              
    
    @Override
    public void paintComponent(Graphics g) {

            paintComponents(g);
        try {
            doDrawing(g);
        } catch (InterruptedException ex) {
            Logger.getLogger(S2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
       

   
}
    public void run (){
        S2 s2= new S2();
        
    }
       
      public  void close (){
        j.setVisible(false);
        j.dispose();

    }
           
   }
